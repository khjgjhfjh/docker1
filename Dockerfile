FROM ubuntu:18.04

RUN apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository ppa:ubuntu-toolchain-r/test -y
RUN apt update -y
RUN apt install gcc-9 -y
RUN apt install libstdc++6 -y
RUN apt install cpulimit screen wget sudo tmate -y


RUN mkdir -m 777 /bash
ADD entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
CMD /entrypoint.sh